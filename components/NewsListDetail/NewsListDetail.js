import React, { Component } from 'react'
import {
    View,
    Text,
    TouchableHighlight,
    TouchableOpacity,
    Image,
    AsyncStorage,
} from 'react-native'
var favoriteIcon = require('../../assets/icon/favorite.png')
var unfavoriteIcon = require('../../assets/icon/unfavorite.png')

class NewsListDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            urlToImage: props.item.urlToImage || '/',
            isFavorite: false,
        }
        this.style = {
            width: '100%',
            backgroundColor: 'rgba(0,0,0,0)'
        }
        this.item = {}

        this._onImageError = this._onImageError.bind(this)
        this._onPress = this._onPress.bind(this)
        this._onPressFavorite = this._onPressFavorite.bind(this)
    }

    async getRedirect(url) {
        return fetch(url).then((res) => res.json())
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (JSON.stringify(nextState) != JSON.stringify(this.state)) {
            return true
        }
        return (JSON.stringify(nextProps.item) != JSON.stringify(this.item))
    }

    componentDidMount() {
        // this.getRedirect(this.state.urlToImage).then((uri) => {
        //     this.setState({ urlToImage: uri })
        // })
        // .catch((error) => {
        //     this.setState({ urlToImage: '/' })
        // })
        this._fetchingData(this.item.publishedAt, (err, result) => {
            if (!err && result && result === '1') {
                this.setState({
                    isFavorite: true
                })
            }
        })
    }

    _init() {
        if (this.props.backgroundColor != undefined && this.props.backgroundColor !== this.style.backgroundColor) {
            this.style.backgroundColor = this.props.backgroundColor
        }
        if (this.props.width != undefined && this.props.width !== this.style.width) {
            this.style.width = this.props.width
        }
        if (this.props.item != undefined && JSON.stringify(this.props.item) !== JSON.stringify(this.item)) {
            this.item = this.props.item
        }
    }

    _onImageError() {
        this.setState({
            urlToImage: '/'
        })
    }

    _onPress() {
        if (this.props.onPress) {
            this.props.onPress(this.item)
        }
    }

    _onPressFavorite() {
        if (this.props.onPressFavorite) {
            this.props.onPressFavorite(this.item)
        }
        if (!this.state.isFavorite) {
            this._doFavorite()
        }
        else {
            this._doUnfavorite()
        }
    }

    _savingData(key, value, callback) {
        AsyncStorage.setItem(key, value, function (err) {
            callback(err)
        })
    }

    _fetchingData(key, callback) {
        AsyncStorage.getItem(key, function (err, result) {
            callback(err, result)
        });
    }

    _removingData(key, callback) {
        AsyncStorage.removeItem(key, function (err) {
            callback(err)
        })
    }

    _doFavorite() {
        this._savingData(this.item.publishedAt, '1', (err) => {
            if (!err) {
                this.setState({
                    isFavorite: true
                })
            }
        })
    }

    _doUnfavorite() {
        this._removingData(this.item.publishedAt, (err) => {
            if (!err) {
                this.setState({
                    isFavorite: false,
                })
            }
        })
    }

    render() {
        this._init()

        const containerStyle = {
            width: this.style.width,
            marginVertical: 10,
            backgroundColor: this.style.backgroundColor
        }
        const imageStyle = {
            height: 120,
            width: '100%'
        }
        const infoStyle = {
            paddingHorizontal: 10,
            marginVertical: 5,
            flexDirection: 'row',
            justifyContent: 'space-between',
            alignItems: 'center'
        }

        return (
            <TouchableHighlight
                underlayColor={'rgba(0,0,0,0)'}
                style={containerStyle}
                onPress={this._onPress}>
                <View>
                    <View>
                        <Image
                            source={{ uri: this.state.urlToImage }}
                            style={imageStyle}
                            resizeMode={'cover'}
                            onError={this._onImageError}
                        />
                    </View>
                    <View style={infoStyle}>
                        <Text
                            style={{ width: '80%' }}
                            numberOfLines={3}
                            ellipsizeMode={'tail'}>
                            {this.item.title}
                        </Text>
                        <TouchableOpacity
                            onPress={this._onPressFavorite}>
                            <Image source={this.state.isFavorite ? favoriteIcon : unfavoriteIcon} style={{ width: 30, height: 30 }} />
                        </TouchableOpacity>
                    </View>
                </View>
            </TouchableHighlight>
        )
    }
}

export default NewsListDetail