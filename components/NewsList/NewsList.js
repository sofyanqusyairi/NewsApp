import React, { Component } from 'react'
import {
    View,
    Text,
    FlatList,
    StyleSheet,
    Dimensions,
    Image,
    ActivityIndicator
} from 'react-native'
import NewsListDetail from '../NewsListDetail/NewsListDetail'

var checkedIcon = require('../../assets/icon/checked.png')
screenWidth = Dimensions.get('window').width
screenHeight = Dimensions.get('window').height

class NewsList extends Component {
    constructor(props) {
        super(props)
        this.style = {
            paddingHorizontal: 0,
        }
        this.items = []
        this.headlineItems = []

        this._handleOnPressItem = this._handleOnPressItem.bind(this)
        this._loadMore = this._loadMore.bind(this)
    }

    shouldComponentUpdate(nextProps, nextState) {
        return (JSON.stringify(nextProps.items) != JSON.stringify(this.items) || JSON.stringify(nextProps.headlineItems) != JSON.stringify(this.headlineItems))
    }

    _init() {
        if (this.props.paddingHorizontal != undefined && this.props.paddingHorizontal != this.style.paddingHorizontal) {
            this.style.paddingHorizontal = this.props.paddingHorizontal
        }
        if (this.props.items != undefined && JSON.stringify(this.props.items) !== JSON.stringify(this.items)) {
            this.items = this.props.items
        }
        if (this.props.headlineItems != undefined && JSON.stringify(this.props.headlineItems) !== JSON.stringify(this.headlineItems)) {
            this.headlineItems = this.props.headlineItems
        }
    }

    _handleOnPressItem(item) {
        if (this.props.onPressItem) {
            this.props.onPressItem(item)
        }
    }

    _loadMore() {
        if (this.props.loadMore) {
            this.props.loadMore()
        }
    }

    render() {
        this._init()

        const containerStyle = {
        }
        const listStyle = {
            paddingHorizontal: this.style.paddingHorizontal,
            backgroundColor: '#ededed',
        }
        const headlineContainerStyle = {
            // borderWidth: 1,
            // borderColor: '#c4c4c4',
            borderBottomWidth: 5,
            borderBottomColor: '#2eacea'
        }

        var _renderSeparator = () => {
            return (
                <View
                    style={{
                        marginVertical: 5,
                        flex: 1,
                        flexDirection: 'row',
                        borderColor: '#c4c4c4',
                        borderWidth: 1
                    }}
                />
            )
        }

        var _renderItemDetail = ({ item, index }) => {
            if (index == 1) {
                return (
                    <View style={[styles.itemDetailContainer, headlineContainerStyle]}>
                        {_renderHeadline()}
                    </View>
                )
            }

            return (
                <View>
                    <View style={[styles.itemDetailContainer, listStyle]}>
                        <NewsListDetail
                            item={item}
                            onPress={this._handleOnPressItem} />
                    </View>
                    {_renderSeparator()}
                </View>
            )
        }

        var _renderHeadlineItemDetail = ({ item, index }) => {
            return (
                <View
                    style={[
                        styles.itemDetailContainer,
                        {
                            borderLeftColor: '#c4c4c4',
                            borderLeftWidth: 1,
                            borderRightColor: '#c4c4c4',
                            borderRightWidth: 1
                        }
                    ]}>
                    <NewsListDetail
                        width={screenWidth}
                        item={item}
                        onPress={this._handleOnPressItem} />
                </View>
            )
        }

        var _renderHeadline = () => {
            return (
                <View>
                    {_renderHeaderHeadlineList()}
                    {
                        this.headlineItems.length > 0 ?
                            (<FlatList
                                horizontal={true}
                                data={this.headlineItems}
                                renderItem={_renderHeadlineItemDetail}
                                bounces={false}
                                keyExtractor={(item, index) => index.toString()}
                                onEndReached={this._loadMore}
                                onEndReachedThreshold={0.5}
                            />)
                            :
                            <View />
                    }
                </View>
            )
        }

        var _renderHeaderHeadlineList = () => {
            return (
                <View style={{
                    paddingHorizontal: 10,
                    flexDirection: 'row',
                    justifyContent: 'flex-end',
                    alignItems: 'center',
                    backgroundColor: '#2eacea'
                }}>
                    <Image source={checkedIcon} style={{ width: 15, height: 15, marginHorizontal: 3 }} />
                    <Text>{'Headline News!'}</Text>
                </View>
            )
        }

        var _renderFooter = () => {
            status = (this.props.isEndOfData != undefined) ? this.props.isEndOfData : true
            if (status) return null

            return (
                <View
                    style={{
                        paddingVertical: 20,
                    }}>
                    <ActivityIndicator animating size='large' />
                </View>
            )
        }

        return (
            <View style={[containerStyle, this.props.containerStyle]}>
                {this.items.length > 0 ?
                    (<FlatList
                        data={this.items}
                        ListFooterComponent={_renderFooter}
                        renderItem={_renderItemDetail}
                        bounces={false}
                        keyExtractor={(item, index) => index.toString()}
                        onEndReached={this._loadMore}
                        onEndReachedThreshold={0.5}
                    />)
                    :
                    <View />
                }
            </View>
        )
    }
}

const styles = StyleSheet.create({
    itemDetailContainer: {
        height: 220,
    }
})

export default NewsList