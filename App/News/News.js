import React, { Component } from 'react'
import {
    View,
    ScrollView,
    StyleSheet,
    ActivityIndicator
} from 'react-native'
import NewsList from '../../components/NewsList/NewsList'
import newslist from '../../assets/json/news_list.json'
NEWS_LIST_PAGE_SIZE = 10
NEWS_HEADLINE_LIST_PAGE_SIZE = 5
API_KEY = '868e94cb8e6a4b929b572120bc012288'
BASE_URL = 'https://newsapi.org/v2/'
KEYWORD = 'bitcoin'
DOMAINS = 'domains=wsj.com,nytimes.com'
COUNTRY = 'us'
NEWS_LIST_URL = BASE_URL + `everything?apiKey=${API_KEY}&pageSize=${NEWS_LIST_PAGE_SIZE}&domains=${DOMAINS}`
NEWS_HEADLINE_LIST_URL = BASE_URL + `top-headlines?apiKey=${API_KEY}&pageSize=${NEWS_HEADLINE_LIST_PAGE_SIZE}&country=${COUNTRY}`
TIMEOUT = 5

class News extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: 'NewsApp',
        }
    }

    constructor(props) {
        super(props)
        this.state = {
            newsList: {},
            newsHeadlineList: {}
        }
        this.newsListPageCount = 1
        this.headlinePageCount = 1
        this.totalNewsListPage = 1
        this.totalHeadlinePage = 1
        this.newsListURL = NEWS_LIST_URL
        this.newsHeadlineListURL = NEWS_HEADLINE_LIST_URL

        this._handleOnPressItem = this._handleOnPressItem.bind(this)
        this._handleLoadMoreNewsList = this._handleLoadMoreNewsList.bind(this)
        this._handleLoadMoreNewsHeadlineList = this._handleLoadMoreNewsHeadlineList.bind(this)
    }

    componentDidMount() {
        // set newslist
        this._changeNewsListURL()
        // set headline
        this._changeNewsHeadlineListURL()

        this._callNewsListAPI('newsList', this.newsListURL)
        this._callNewsHeadlineListAPI('newsHeadlineList', this.newsHeadlineListURL)
    }

    _changeNewsListURL() {
        this.newsListURL = `${NEWS_LIST_URL}&page=${this.newsListPageCount}`
    }

    _changeNewsHeadlineListURL() {
        this.newsHeadlineListURL = `${NEWS_HEADLINE_LIST_URL}&page=${this.headlinePageCount}`
    }

    _callNewsListAPI(stateName, apiURL) {
        fetch(apiURL,
            {
                method: 'GET', timeout: TIMEOUT
            }).
            then((response) => response.json()).
            then((responseJson) => {
                this.totalNewsListPage = Math.ceil(responseJson.totalResults / NEWS_LIST_PAGE_SIZE)
                if (responseJson.articles.length > 0) {
                    if (this.state[stateName].length > 0) {
                        this.setState({
                            [stateName]: [...this.state[stateName], ...responseJson.articles]
                        })
                    }
                    else {
                        this.setState({
                            [stateName]: responseJson.articles
                        })
                    }
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    _callNewsHeadlineListAPI(stateName, apiURL) {
        fetch(apiURL,
            {
                method: 'GET', timeout: TIMEOUT
            }).
            then((response) => response.json()).
            then((responseJson) => {
                if (responseJson.articles.length > 0) {
                    this.setState({
                        [stateName]: responseJson.articles
                    })
                }
            })
            .catch((error) => {
                console.log(error);
            });
    }

    _handleOnPressItem(item) {
        this.props.navigation.navigate('NewsWebView', { title: item.title, url: item.url })
    }

    _handleLoadMoreNewsList() {
        this.newsListPageCount++
        if (this.newsListPageCount <= this.totalNewsListPage) {
            this._changeNewsListURL()
            console.log('url:', this.newsListURL)
            this._callNewsListAPI('newsList', this.newsListURL)
        }
    }

    _handleLoadMoreNewsHeadlineList() {
        this.headlinePageCount++
        if (this.headlinePageCount <= this.totalHeadlinePage) {
            this._changeNewsHeadlineListURL()
            console.log('url:', this.newsHeadlineListURL)
            this._callNewsHeadlineListAPI('newsHeadline', this.newsHeadlineListURL)
        }
    }

    render() {
        let newsList = this.state.newsList
        if (Object.keys(newsList).length === 0 && newsList.constructor === Object) {
            return (
                <View
                    style={{
                        paddingVertical: 20,
                        borderTopWidth: 1,
                        borderColor: '#CED0CE',
                    }}>
                    <ActivityIndicator animating size='large' />
                </View>
            )
        }

        return (
            <View>
                <NewsList
                    paddingHorizontal={10}
                    onPressItem={this._handleOnPressItem}
                    items={this.state.newsList}
                    headlineItems={this.state.newsHeadlineList}
                    loadMore={this._handleLoadMoreNewsList}
                    isEndOfData={this.newsListPageCount === this.totalNewsListPage}
                />
            </View>
        )
    }
}

export default News