import React from 'react'
import { View, Text } from 'react-native'
import { StackNavigator } from 'react-navigation'
import News from './News/News'
import NewsWebView from './NewsWebView/NewsWebView'

export default StackNavigator({
    News: {
        screen: News,
    },
    NewsWebView: {
        screen: NewsWebView,
    },
})