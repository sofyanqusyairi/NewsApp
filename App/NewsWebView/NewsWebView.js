import React, { Component } from 'react'
import { WebView } from 'react-native'

class NewsWebView extends Component {
    static navigationOptions = ({ navigation }) => {
        return {
            title: navigation.state.params.title
        }
    }

    constructor(props) {
        super(props)
    }
    render() {
        return (
            <WebView
                source={{ uri: this.props.navigation.state.params.url }}
                style={{ marginTop: 20, }}
            />
        )
    }
}

export default NewsWebView